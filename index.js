/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
function userInfo(){
	let name = prompt("What is your name?");
	let age = prompt("How old are you?");
	let address = prompt("Where do you live?");
	alert("Thanks for the input");
	console.log("Hello, "+ name);
	console.log("You are "+ age + "years old.");
	console.log("You live in "+ address);
}

userInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favBands(){
		console.log("1. When Chai Met Toast");
		console.log("2. Sanam");
		console.log("3. Coldplay");
		console.log("4. The Beatles");
		console.log("5. Metallica");
	}
	favBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function favMovies(){
		console.log("1. The Pursuit of Happyness \nRotten Tomatoes Rating: 67%");
		console.log("1. Iron Man \nRotten Tomatoes Rating: 94%");
		console.log("1. Kimi no na wa \nRotten Tomatoes Rating: 98%");
		console.log("1. 3 Idiots \nRotten Tomatoes Rating: 100%");
		console.log("1. Jujutsu Kaisen 0 \nRotten Tomatoes Rating: 98%");
	}
	favMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3);
};

printFriends();
